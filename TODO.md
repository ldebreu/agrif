# AGRIF List of important TODO

---

## conv / library

1. Do not use hard-coded length of character variables (5000). See agrif_variable_c type in modtypes.F. We should always use allocatable arrays.It seems to be now possible with Fortran 2008 and beyond. Note however that the conv currently produces a warning. Should it be a STOP ?

---

## Main files

1. Agrif2Model.F (and .F90) must be independent of the free/fixed form and most importantly independent of the target code. This shoud be the user responsability to ensure that this file is compiled with the right preprocessing options.